//
//  OrderDocViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit
import Eureka

class OrderDocViewController: FormViewController  {
 
    var fullProfile : FullProfile!
    var selectedList : List!
    var cellArray = Array<OrderCellObjectProtocol>()
    var titleRow: BaseRow!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let jsonString = UserDefaults.standard.string(forKey: "FullProfileJson")
        self.fullProfile = FullProfile(JSONString :jsonString ?? "")
        self.selectedList = (fullProfile.spravki?.Content.List![0])!
        title = "Новая справка"
        buildCellObjects()
    }
       
    func dismissPickerView() {
//        let toolBar = UIToolbar()
//        toolBar.sizeToFit()
//        let button = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(self.action))
//        toolBar.setItems([button], animated: true)
//        toolBar.isUserInteractionEnabled = true
//        typeTextField.inputAccessoryView = toolBar
       }
    
    @objc func action() {
       view.endEditing(true)
    }
    
    override func valueHasBeenChanged(for: BaseRow, oldValue: Any?, newValue: Any?) {
        if `for` == titleRow {
            let items = (fullProfile.spravki?.Content.List)!
            for (index, item) in items.enumerated() {
                if item.name == newValue as! String {
                    selectedList = item
                    buildCellObjects()
                    return
                }
            }
        }
    }
    
    func buildCellObjects() {
        cellArray.removeAll()
        form.removeAll()
        var lastSection = Section("Выберите вид справки")
        form +++ lastSection
        titleRow = ActionSheetRow<String>(){
            $0.title = selectedList.name
            let items = (fullProfile.spravki?.Content.List)!
            $0.options = items.map({ (list) -> String in
                list.name
            })
            
        }
        lastSection <<< titleRow
//        lastSection = Section("Заполните данные")
//        form +++ lastSection
        for input in (fullProfile.spravki?.Content.Forms[selectedList.id]?.Inputs)! {
            if !input.readonly{
            switch input.type {
            case "Catalog":
                let orderTitleObject = CatalogObject()
                orderTitleObject.title = input.label
                orderTitleObject.id = input.fieldId
                orderTitleObject.type = input.type
                cellArray.append(orderTitleObject)
                let items = (fullProfile.spravki?.Content.Forms[selectedList.id]?.Refs[input.ref])!
                for raw in items {
                    let orderCheckObject = OrderCheckObject()
                    orderCheckObject.title = raw.value
                    orderCheckObject.type = input.type
                    orderCheckObject.id = raw.id
                    orderTitleObject.items.append(orderCheckObject)

                    
                }
                lastSection <<< ActionSheetRow<String>(){
                    $0.title = input.label
                    $0.options = items.map({ (object) -> String in
                        object.value
                    })
                }.onChange({ (row) in
                    let selected = orderTitleObject.items.filter { (object) -> Bool in
                        return object.title == row.value
                    }[0]
                    orderTitleObject.selected = selected
                })
                break
                                
            case "RadioButton":
                let orderTitleObject = OrderTitleObject()
                orderTitleObject.title = input.label
                orderTitleObject.id = input.fieldId
                orderTitleObject.type = input.type
                cellArray.append(orderTitleObject)
                lastSection = Section(input.label)
                form +++ lastSection
                for raw in (fullProfile.spravki?.Content.Forms[selectedList.id]?.Refs[input.ref])! {
                    let orderCheckObject = OrderCheckObject()
                    orderCheckObject.title = raw.value
                    orderCheckObject.type = input.type
                    orderCheckObject.id = raw.id
                    orderTitleObject.items.append(orderCheckObject)

                    lastSection <<< SwitchRow(){
                        $0.title = raw.value
                    }.onChange({ (row) in
                        orderCheckObject.isChecked = row.cell.switchControl.isOn
                    })
                }
//                let row = MultipleSelectorRow<String>(){
//                    $0.title = input.label
//                    let items = (fullProfile.spravki?.Content.Forms[selectedList.id]?.Refs[input.ref])!
//                    $0.options = items.map({ (raw) -> String in
//                        raw.value
//                    })
//                }.onCellSelection({ (cell, row) in
//
//                })
//                lastSection <<< row

                lastSection = Section("")
                form +++ lastSection
                break
            case "Checkmark":
                let orderCheckObject = OrderCheckObject()
                orderCheckObject.title = input.label
                orderCheckObject.type = input.type
                orderCheckObject.id = input.fieldId
                cellArray.append(orderCheckObject)
                lastSection <<< SwitchRow(){
                    $0.title = input.label 
                }.onChange({ (row) in
                    orderCheckObject.isChecked = row.value ?? false
                })
                break
            case "Date":
                let orderTextObj = OrderTextObject()
                orderTextObj.title = input.label
                orderTextObj.type = input.type
                orderTextObj.id = input.fieldId
                cellArray.append(orderTextObj)
                lastSection <<< DateRow(){
                    $0.title = input.label
                    
                }.onChange({ (row) in
                    let date = row.value as Date?
                    orderTextObj.inputText = date?.toDotted() ?? ""
                })
                                break
            case "Text":
                let orderTextObj = OrderTextObject()
                orderTextObj.title = input.label
                orderTextObj.type = input.type
                orderTextObj.id = input.fieldId
                cellArray.append(orderTextObj)
                lastSection <<< TextAreaRow(){
                    $0.placeholder = input.label
                }.onChange({ (row) in
                    orderTextObj.inputText = row.value ?? ""
                })
                break
            case "Number":
                let orderTextObj = OrderTextObject()
                orderTextObj.title = input.label
                orderTextObj.type = input.type
                orderTextObj.id = input.fieldId
                cellArray.append(orderTextObj)
                lastSection <<< IntRow(){
                    $0.title = input.label
                }.onChange({ (row) in
                    orderTextObj.inputText = String(row.value ?? 0)
                })
                break
            case "Phone":
                let orderTextObj = OrderTextObject()
                orderTextObj.title = input.label
                orderTextObj.type = input.type
                orderTextObj.id = input.fieldId
                cellArray.append(orderTextObj)
                lastSection <<< PhoneRow(){
                    $0.title = input.label
                }.onChange({ (row) in
                    orderTextObj.inputText = row.value ?? ""
                })
                break
            case "Email":
                let orderTextObj = OrderTextObject()
                orderTextObj.title = input.label
                orderTextObj.type = input.type
                orderTextObj.id = input.fieldId
                cellArray.append(orderTextObj)
                lastSection <<< EmailRow(){
                    $0.title = input.label
                }.onChange({ (row) in
                    orderTextObj.inputText = row.value ?? ""
                })
                break
            default:
                break
            }
            }
        }
        
    }
    @IBAction func doneAction(_ sender: Any) {
        var spravkaDic = Dictionary<String,Any>()
        var typeString = ""
        for list in (fullProfile.spravki?.Content.List)! {
            if list.id == selectedList.id {
                typeString = list.type
            }
        }
        spravkaDic["Type"] = typeString
        spravkaDic["Login_AD"] = (UserDefaults.standard.string(forKey: "loginAD") ?? "")
        var responceDictionary = Dictionary<String,Any>()
        responceDictionary["form_id"] = selectedList.id
//        responceDictionary["form_id"] = "680984"
        var fieldsAr = Array<Any>()
        for item in cellArray {
            var smallDic = Dictionary<String,Any>()
            smallDic["id"] = String(item.getId())
            if item.getType() == "RadioButton" {
                var smallSmallDic = Dictionary<String,[String]>()
                smallSmallDic["choice_ids"] = item.getValues()
                smallDic["value"] = smallSmallDic
            } else if item.getType() == "Checkmark" {
                smallDic["value"] = item.getValues()[0] == "true" ? "checked" : "unchecked"
            } else if item.getType() == "Catalog" {
                var smallSmallDic = Dictionary<String, String>()
                let values = item.getValues()
                var selected: String?
                if !values.isEmpty {
                    selected = values[0]
                }
                if selected != nil {
                    smallSmallDic["item_id"] = selected
                    smallDic["value"] = smallSmallDic
                } else {
                    continue
                }
                
            } else{
                smallDic["value"] = item.getValues()[0]
            }
            fieldsAr.append(smallDic)
        }
        responceDictionary["fields"] = fieldsAr
        spravkaDic["Val"] = responceDictionary
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: spravkaDic,
            options: []) {
            let theJSONText = "Data=" + (String(data: theJSONData,
                                                encoding: .ascii) ?? "")
            print("JSON string = \(theJSONText)")
            let spravkiPost = SpravkiPost()
            doneButton.isEnabled = false
            spravkiPost.postData(json: theJSONText) { (title, message, success) in
                self.doneButton.isEnabled = true
                self.alert(title: title, message: message, completion: {
                    if success {
                        self.navigationController?.popViewController(animated: true)
                    }
                })
                
            }
            
        }
    }
}
