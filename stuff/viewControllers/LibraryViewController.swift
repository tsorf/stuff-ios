//
//  LibraryViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit
import SwiftSVG

class LibraryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var libraryTableView: UITableView!
    
    var fullProfile : FullProfile!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let jsonString = UserDefaults.standard.string(forKey: "FullProfileJson")
        self.fullProfile = FullProfile(JSONString :jsonString ?? "")
        libraryTableView.dataSource = self
        libraryTableView.delegate = self
        title = "Библиотека"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fullProfile.books.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectedKey = Array(self.fullProfile.books.keys)[indexPath.row]
        let bookForCell = self.fullProfile.books[selectedKey]
        let cell:LibraryCell = (self.libraryTableView.dequeueReusableCell(withIdentifier: "Cell") as! LibraryCell?)!
        cell.titleLabel.text = bookForCell?.title
        cell.icon?.isHidden = true
        cell.libraryImageView.downloaded(from:bookForCell?.img ?? "")
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let selectedKey = Array(self.fullProfile.books.keys)[indexPath.row]
        let bookForCell = self.fullProfile.books[selectedKey]
        guard let url = URL(string: bookForCell?.href ?? "") else { return }
        UIApplication.shared.open(url)
    }
    
}
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    } 
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }

}

extension UIView {
    func downloadedSvg(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        subviews.forEach { $0.removeFromSuperview() }
        DispatchQueue.main.async() {
            let image = UIView(SVGURL: url) { (svgLayer) in
                svgLayer.fillColor = UIColor(red:0.52, green:0.16, blue:0.32, alpha:1.00).cgColor
                svgLayer.resizeToFit(self.bounds)
            }
            self.addSubview(image)
        }
        
    }

}
