//
//  SpravkiViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 27.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit


class SpravkiViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,
ProfileProtocol {
    func didCompletedProfileGet(fullProfile: FullProfile) {
        self.fullProfile = fullProfile
        refreshControl.endRefreshing()
        spravkiTableView.reloadData()
    }
    
    func didCompleteProfileGetWithError() {
        refreshControl.endRefreshing()
        alert(title: "Ошибка", message: "Что-то пошло не так")
    }
    
    
    var fullProfile : FullProfile!
    private let refreshControl = UIRefreshControl()
    @IBOutlet weak var spravkiTableView: UITableView!
    private let profileGet: ProfileGet = ProfileGet()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Заказ справок"
        profileGet.delegate = self
        refreshControl.attributedTitle = NSAttributedString(string: "Обновление")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        spravkiTableView.addSubview(refreshControl)
    }
    
    @objc private func refresh() {
        profileGet.getProfile()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let jsonString = UserDefaults.standard.string(forKey: "FullProfileJson") ?? ""
        self.fullProfile = FullProfile(JSONString :jsonString )
        spravkiTableView.dataSource = self
        spravkiTableView.delegate = self
        spravkiTableView.reloadData()
        refresh()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fullProfile.spravki?.Ordered.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.spravkiTableView.dequeueReusableCell(withIdentifier: "Cell") as! SpravkiCell
        cell.titleLabel.text = fullProfile.spravki?.Ordered[indexPath.row].name
        cell.dateLabel.text = fullProfile.spravki?.Ordered[indexPath.row].date
        cell.statusLabel.text = fullProfile.spravki?.Ordered[indexPath.row].status
        cell.selectionStyle = .none
        return cell
    }
}
