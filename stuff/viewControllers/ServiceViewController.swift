//
//  ServiceViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome_swift

class ServiceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var serviceTableView: UITableView!
    
    var fullProfile : FullProfile!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let jsonString = UserDefaults.standard.string(forKey: "FullProfileJson")
        self.fullProfile = FullProfile(JSONString :jsonString ?? "")
        serviceTableView.dataSource = self
        serviceTableView.delegate = self
        title = "Сервисы академии"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return self.fullProfile.links.count
       }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectedKey = Array(self.fullProfile.links.keys)[indexPath.row]
        let bookForCell = self.fullProfile.links[selectedKey]
        let cell:LibraryCell = (self.serviceTableView.dequeueReusableCell(withIdentifier: "Cell") as! LibraryCell?)!
        cell.titleLabel.text = bookForCell?.title
        let strIcon = "\\" + String(bookForCell?.icon.dropFirst() ?? "")
         
        cell.imageView?.isHidden = true
        if let icon = bookForCell?.icon {
            cell.icon.isHidden = false
            cell.icon.downloadedSvg(from: URL(string: icon)!)
        } else {
            cell.icon.isHidden = true
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let selectedKey = Array(self.fullProfile.links.keys)[indexPath.row]
        let bookForCell = self.fullProfile.links[selectedKey]
        guard let url = URL(string: bookForCell?.href ?? "") else { return }
        UIApplication.shared.open(url)
    }
}
