//
//  TrudViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 29.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class TrudViewController: UIViewController, WKNavigationDelegate {
    
    var fullProfile : FullProfile!
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Трудоустройство"
        let jsonString = UserDefaults.standard.string(forKey: "FullProfileJson")
        self.fullProfile = FullProfile(JSONString :jsonString ?? "")
        let htmlString:String! = "\(self.fullProfile.trud!)"
        webView.navigationDelegate = self
        webView.loadHTMLString(htmlString, baseURL: Bundle.main.bundleURL)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      
    }

}
