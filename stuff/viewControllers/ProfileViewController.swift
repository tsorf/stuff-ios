//
//  ProfileViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 20.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class ProfileViewController: UIViewController, ProfileProtocol, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var profileTableView: UITableView!
    
    var cellArray = Array<ProfileCellObject>()
    
    var fullProfile : FullProfile!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let stringFull = UserDefaults.standard.string(forKey: "FullProfileJson") ?? ""
        if stringFull == "" || stringFull.contains("Too many attempts"){
            let profileGet = ProfileGet()
            profileGet.delegate = self
            profileGet.getProfile()
            activityIndicator.isHidden = false
            UIApplication.shared.beginIgnoringInteractionEvents()
        }else{
            self.fullProfile = FullProfile(JSONString :stringFull )
            activityIndicator.isHidden = true
            fillField()
        }
        title = "Профиль"
    }
    func didCompletedProfileGet(fullProfile : FullProfile) {
        self.fullProfile = fullProfile
        activityIndicator.isHidden = true
        fillField()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
      
    func didCompleteProfileGetWithError() {
        activityIndicator.isHidden = true
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    func fillField() {
        profileTableView.delegate = self
        profileTableView.dataSource = self
        let url = URL(string: fullProfile.profile?.Photo ?? "")
        avatarImageView.kf.setImage(with: url, placeholder: UIImage(named: "profile1"))
        name.text = (fullProfile.profile?.Familiya ?? "") + " " + (fullProfile.profile?.Imya ?? "")
        email.text = fullProfile.profile?.Email
        avatarImageView.layer.borderWidth = 0.5
        avatarImageView.layer.masksToBounds = false
        avatarImageView.layer.borderColor = UIColor.lightGray.cgColor
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height/2
        avatarImageView.clipsToBounds = true
        let profile = fullProfile.profile
        let arDelo = profile!.Ar_Delo![0]
        let obj = ProfileCellObject()
        obj.title = "Подразделение"
        obj.subTitle = arDelo.Struct_Podr
        cellArray.append(obj)
        let obj1 = ProfileCellObject()
        obj1.title = "Отдел"
        obj1.subTitle = arDelo.Department
        cellArray.append(obj1)
        let obj2 = ProfileCellObject()
        obj2.title = "Должность"
        obj2.subTitle = arDelo.POS
        cellArray.append(obj2)
        profileTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellObject = cellArray[indexPath.row]
        let cell = self.profileTableView.dequeueReusableCell(withIdentifier: "Cell") as! SpravkiCell
        cell.titleLabel.text = cellObject.title
        cell.dateLabel.text = cellObject.subTitle
        return cell as UITableViewCell
    }
}

class ProfileCellObject {
    var title: String = ""
    var subTitle: String = ""
}
