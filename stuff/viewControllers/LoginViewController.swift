//
//  LoginViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 20.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate,ApiProtocol {
    func didCompletedLoginAutorisation() {
        DispatchQueue.main.async {
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainController")
            vc.modalPresentationStyle = .fullScreen
            let sceneDelegate = self.view.window?.windowScene?.delegate as! SceneDelegate
            sceneDelegate.window?.rootViewController = vc
        }
        
    }
    
    func didCompleteLoginAutorisationWithError() {
        let alert = UIAlertController(title: "Ошибка авторизации", message: "Попробуйте еще раз", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
    }

    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        loginTextField.delegate = self
        passTextField.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
            moveTextField(textfield: textField, moveDistance: -216, up: true)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
            moveTextField(textfield: textField, moveDistance: -216, up: false)
    }

    func moveTextField(textfield: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance: -moveDistance)
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    @IBAction func doneButtonAction(_ sender: Any) {
        let registrationGet = RegistrationGet()
        registrationGet.delegate = self
        registrationGet.registration(login: loginTextField.text!, pass: passTextField.text!)
    }
    
   
}
