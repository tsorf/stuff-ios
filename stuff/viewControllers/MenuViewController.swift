//
//  MenuViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var menuItems = Array<String>()
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let jsonString = UserDefaults.standard.string(forKey: "FullProfileJson")
        let fullProfile = FullProfile(JSONString :jsonString ?? "")
        myTableView.dataSource = self
        myTableView.delegate = self
        menuItems.append("redButton")
        menuItems.append("service")
        menuItems.append("library")
        if fullProfile?.aup != nil {
            menuItems.append("improveList")
        }
        if fullProfile?.trud != nil {
            menuItems.append("trud")
        }
        menuItems.append("quit")
        title = "Меню"
    }

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
   }

   // create a cell for each table view row
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellIdentifier = menuItems[indexPath.row]
    
    let cell:UITableViewCell = (self.myTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell?)!
        
    cell.selectionStyle = .none
       return cell
   }

   // method to run when table view cell is tapped
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    let cell = tableView.cellForRow(at: indexPath)
    if(cell?.reuseIdentifier == "quit"){
        cell?.selectionStyle = .none
        UserDefaults.standard.set(false, forKey: "authenticatedUser") //Bool
        UserDefaults.standard.set("", forKey: "FullProfileJson")
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
        vc.modalPresentationStyle = .fullScreen 
        let sceneDelegate = self.view.window?.windowScene?.delegate as! SceneDelegate
        sceneDelegate.window?.rootViewController = vc
        sceneDelegate.window?.makeKeyAndVisible()
    }
    
   }

}
