//
//  ImproveViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class ImproveViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    var fullProfile : FullProfile!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Аттестационный лист"
        let jsonString = UserDefaults.standard.string(forKey: "FullProfileJson")
        self.fullProfile = FullProfile(JSONString :jsonString ?? "")
        let htmlString:String! = "\(self.fullProfile.aup!)"
        webView.navigationDelegate = self
        webView.loadHTMLString(htmlString, baseURL: Bundle.main.bundleURL)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      
    }

}
