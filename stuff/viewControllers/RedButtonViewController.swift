//
//  RedButtonViewController.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit
import Eureka

class RedButtonViewController: FormViewController {
     
    var cellArray = Array<OrderCellObjectProtocol>()
    var fullProfile : FullProfile!
    var lastSection: Section!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let jsonString = UserDefaults.standard.string(forKey: "FullProfileJson")
        self.fullProfile = FullProfile(JSONString :jsonString ?? "")
        buildCellObjects()
        title = "Красная кнопка"
    }
    
    func buildCellObjects() {
        cellArray.removeAll()
        form.removeAll()
        lastSection = Section("Заполните данные")
        form +++ lastSection
        if(fullProfile.redButton != nil){
        for input in (fullProfile.redButton!.GetForm.Inputs)! {
            if !input.readonly{
            switch input.type {
            case "Text":
                let orderTextObj = OrderTextObject()
                orderTextObj.title = input.label
                orderTextObj.type = input.type
                orderTextObj.id = input.fieldId
                cellArray.append(orderTextObj)
                lastSection <<< TextAreaRow(){
                    $0.placeholder = input.label
                }.onChange({ (row) in
                    orderTextObj.inputText = row.value ?? ""
                })
                break
            case "Catalog":
                let orderTitleObject = CatalogObject()
                orderTitleObject.title = input.label
                orderTitleObject.id = input.fieldId
                orderTitleObject.type = input.type
                cellArray.append(orderTitleObject)
                let items = (fullProfile.redButton!.GetForm.Refs[input.ref])!
                for raw in items {
                    let orderCheckObject = OrderCheckObject()
                    orderCheckObject.title = raw.value
                    orderCheckObject.type = input.type
                    orderCheckObject.id = raw.id
                    orderTitleObject.items.append(orderCheckObject)

                    
                }
                lastSection <<< ActionSheetRow<String>(){
                    $0.title = input.label
                    $0.options = items.map({ (object) -> String in
                        object.value
                    })
                }.onChange({ (row) in
                    let selected = orderTitleObject.items.filter { (object) -> Bool in
                        return object.title == row.value
                    }[0]
                    orderTitleObject.selected = selected
                })
                break

            default:
                break
            }
            }
        } 
        }
    }
    @IBAction func doneAction(_ sender: Any) {
        var responceDictionary = Dictionary<String,Any>()
        let formId = String(fullProfile.redButton!.GetForm.FormId)
        responceDictionary["form_id"] = formId
        var fieldsAr = Array<Any>()
        for item in cellArray {
            var smallDic = Dictionary<String,Any>()
            smallDic["id"] = String(item.getId())
            if item.getType() == "Catalog" {
                var smallSmallDic = Dictionary<String, String>()
                let values = item.getValues()
                var selected: String?
                if !values.isEmpty {
                    selected = values[0]
                }
                if selected != nil && !selected!.isEmpty {
                    smallSmallDic["item_id"] = selected
                    smallDic["value"] = smallSmallDic
                } else {
                    continue
                }
                
            }  else if item.getType() == "Checkmark" {
                smallDic["value"] = item.getValues()[0] == "true" ? "checked" : "unchecked"
            }  else{
                smallDic["value"] = item.getValues()[0]
            }
            fieldsAr.append(smallDic)
        }
        responceDictionary["fields"] = fieldsAr
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: responceDictionary,
            options: []) {
            let theJSONText = "Data=" + (String(data: theJSONData,
                                                encoding: .ascii) ?? "")
            print("JSON string = \(theJSONText)")
            let redButtonPost = RedButtonPost()
            doneButton.isEnabled = false
            redButtonPost.postData(json: theJSONText) { (title, message) in
                self.doneButton.isEnabled = true
                self.alert(title: title, message: message)
            }
            
        }
    }
}
