//
//  RedButtonPost.swift
//  stuff
//
//  Created by Victor Roslyakov on 26.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation

class RedButtonPost : NSObject {
    
    open func postData(json : String,  callback: @escaping(String, String) -> Void){
        
        var urlString = "https://lk-mobile.ranepa.ru/"+(UserDefaults.standard.string(forKey: "did") ?? "") + "/StaffIOS.redButtonPostTask?key="
        urlString = urlString + (UserDefaults.standard.string(forKey: "publicKey") ?? "")
        
        let url = NSURL(string: urlString)
        var request = URLRequest(url: url! as URL)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = Data(json.utf8)

        let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in

            DispatchQueue.main.async {
                if error != nil {
                    callback("Ошибка", error?.localizedDescription ?? "Произошла ошибка")
                } else {
                    let jsonString = String(data: data!, encoding: String.Encoding.utf8)
                    if let json = jsonString, let result = fromJson(text: json) {
                        if (result["data"]is NSNull) {
                            callback("Ошибка", result["message"] as? String ?? "Произошла ошибка")
                        } else {
                            callback("Готово", "Ваша заявка успешно отправлена")
                        }
                    } else {
                        callback("Ошибка", "Произошла ошибка")
                    }
                }
            }
        });

        task.resume()
        
    }

}
