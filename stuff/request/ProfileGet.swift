//
//  ProfileGet.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation

class ProfileGet : NSObject {
    
    open var delegate : ProfileProtocol!
    
    open func getProfile(){
        
        var urlString = "https://lk-mobile.ranepa.ru/"+(UserDefaults.standard.string(forKey: "did") ?? "") + "/StaffIOS.getProfile?key="
        urlString = urlString + (UserDefaults.standard.string(forKey: "publicKey") ?? "") + "&Login_AD=" + (UserDefaults.standard.string(forKey: "loginAD") ?? "")
        
        let url = NSURL(string: urlString)
        let request = NSURLRequest(url: url! as URL)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in

            DispatchQueue.main.async {
                if error != nil {
                    self.delegate.didCompleteProfileGetWithError()
                } else {
                    let jsonString = String(data: data!, encoding: String.Encoding.utf8)
                    if let json = jsonString, let result = fromJson(text: json) {
                        if (result["data"]is NSNull) {
                            self.delegate.didCompleteProfileGetWithError()
                        } else {
                            let jsonString = String(data: data!, encoding: String.Encoding.utf8)
                            UserDefaults.standard.set(jsonString, forKey: "FullProfileJson")
                            let fullProfile = FullProfile(JSONString :jsonString ?? "")

                            self.delegate.didCompletedProfileGet(fullProfile: fullProfile!)
                        }
                    } else {
                        self.delegate.didCompleteProfileGetWithError()
                    }
                }
            }
            
            
        });

        task.resume()
        
    }

}

public protocol ProfileProtocol{
    func didCompletedProfileGet(fullProfile : FullProfile)
    func didCompleteProfileGetWithError()
}
