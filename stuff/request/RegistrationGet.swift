//
//  RegistrationGet.swift
//  stuff
//
//  Created by Victor Roslyakov on 21.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift

class RegistrationGet : NSObject {
    
    open var delegate:ApiProtocol!
    
    let appCode = "com.ranepa.staff-ios"
    let simCode = "HqRYTJgmxhUrf9QG"
    let did = UIDevice.current.identifierForVendor?.uuidString
    let random = String(Int.random(in: 100000..<999999))
    
    open func registration(login : String, pass : String){
        let loginDict = ["uid": login, "ups": pass,"did":did,"aid":appCode,"rnd":random]
        let encoder = JSONEncoder()
        var jsonStr = ""
        if let jsonData = try? encoder.encode(loginDict) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                jsonStr = jsonString
            }
        }
        let aesEncrJson = try! jsonStr.aesEncrypt(key: simCode)
        let urlEncoded = aesEncrJson.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let urlString = "https://lk-mobile.ranepa.ru/hello.StaffIOS?x="+urlEncoded
        let url = NSURL(string: urlString)
        let request = NSURLRequest(url: url! as URL)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            guard let dictionary = try! JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] else {
                print("Could not cast JSON content as a Dictionary<String, Any>")
                return
            }
            let auth : NSNumber = dictionary["auth"] as! NSNumber
            if(auth == 0){
                self.delegate.didCompleteLoginAutorisationWithError()
                return
            }
            let concRnd = (Int(self.random) ?? 0) + (dictionary["data"] as! Int)
            let stringForMD5 : String = self.did! + String(concRnd) + login + String(concRnd) + self.appCode + self.did!
            let md5String = stringForMD5.md5()
            UserDefaults.standard.set(md5String, forKey: "publicKey")
            UserDefaults.standard.set(self.did, forKey: "did")
            UserDefaults.standard.set(login, forKey: "loginAD")
            UserDefaults.standard.set(true, forKey: "authenticatedUser") //Bool
            DispatchQueue.main.async {
                self.delegate.didCompletedLoginAutorisation()
            }
        });

        task.resume()
        
    }
//    open func autorisation(){
//        let newAppCode = "StaffIOS-"+did!
//        let newAppCodeMD5 = newAppCode.md5()
//        let lenth = newAppCodeMD5.count - 16
//        let sim = newAppCodeMD5.suffix(lenth)
//        let apocalypse = 1356078072
//        let priority = Int.random(in: 1..<8)
//        let moment = Date.init().timeIntervalSince1970 as! Int
//        let repeater = (priority + moment)
//        let befor = repeater/2
//        let after = repeater - befor
//        let regid = newAppCode
//    }
}

public protocol ApiProtocol{
    func didCompletedLoginAutorisation()
    func didCompleteLoginAutorisationWithError()
}

extension String {
    func aesEncrypt(key: String) throws -> String {

        var result = ""

        do {
            let key: [UInt8] = Array(key.utf8) as [UInt8]
            let aes = try! AES(key: key, blockMode: ECB() , padding:.pkcs5) // AES128 .ECB pkcs7
            let encrypted = try aes.encrypt(Array(self.utf8))
            result = encrypted.toBase64()!
            print("AES Encryption Result: \(result)")
        } catch {
            print("Error: \(error)")
        }

        return result
    }
}

