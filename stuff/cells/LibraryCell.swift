//
//  LibraryCell.swift
//  stuff
//
//  Created by Victor Roslyakov on 25.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit

class LibraryCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var libraryImageView: UIImageView!
    @IBOutlet weak var icon: UIView!
}
