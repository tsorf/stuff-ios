//
//  RedTextViewCell.swift
//  stuff
//
//  Created by Victor Roslyakov on 29.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit

class RedTextViewCell: UITableViewCell , OrderCellProtocol,UITextViewDelegate{

    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var titieLabel: UILabel!
    
    var redTextViewObj : RedTextViewObject!
    
    func updateCellWithObject(object: Any) {
        descTextView.delegate = self
        redTextViewObj = object as? RedTextViewObject
        descTextView.layer.borderColor = UIColor.lightGray.cgColor
        descTextView.layer.borderWidth = 1
        titieLabel.text = redTextViewObj.title
    }
    func textViewDidEndEditing(_ textView: UITextView){
        redTextViewObj.inputText = descTextView.text
    }
    func textViewDidBeginEditing(_ textView: UITextView){
        redTextViewObj.inputText = descTextView.text
    }
    func textViewDidChange(_ textView: UITextView){
        redTextViewObj.inputText = descTextView.text
    }
    
}
class RedTextViewObject: OrderCellObjectProtocol{
    
    var inputText = ""
    var title :String = ""
    var isChecked = false
    var id = 0
    var type = ""
    
    func getIdentifire() -> String {
        "RedTextViewCell"
    }
    
    func getType() -> String {
        type
    }
    
    func getValues() -> [String] {
        [inputText]
    }
    
    func getId() -> Int {
        id
    }
    
    
}
