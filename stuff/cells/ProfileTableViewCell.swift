//
//  ProfileTableViewCell.swift
//  stuff
//
//  Created by Victor Roslyakov on 27.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit

class ProfileTableViewCell: UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
}

class ProfileCellObject {
    var title: String = ""
    var subTitle: String = ""
}
