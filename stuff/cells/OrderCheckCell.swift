//
//  OrderCheckCell.swift
//  stuff
//
//  Created by Victor Roslyakov on 26.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit

class OrderCheckCell: UITableViewCell, OrderCellProtocol {
    
    @IBOutlet weak var titleCheck: UILabel!
    @IBOutlet weak var titleSwitch: UISwitch!
    
    var orderCheckCell : OrderCheckObject!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
    }
    func updateCellWithObject(object: Any) {
        orderCheckCell = object as? OrderCheckObject
        titleCheck.text = orderCheckCell.title
    }
    @objc func switchChanged(mySwitch: UISwitch) {
        orderCheckCell.isChecked = mySwitch.isOn
    }
}


open class OrderCheckObject: NSObject , OrderCellObjectProtocol {
    
    let identifire = "OrderCheckCell"
    var title :String = ""
    var isChecked = false
    var id = 0
    var type = ""
    
    public func getIdentifire() -> String {
        return identifire
    }
    public func getType()-> String{
        return type
    }
    public func getValues()-> [String]{
        return [String(isChecked)]
    }
    public func getId()-> Int{
        return id
    }
}
