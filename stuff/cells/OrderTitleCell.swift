//
//  OrderTitleCell.swift
//  stuff
//
//  Created by Victor Roslyakov on 26.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit

class OrderTitleCell: UITableViewCell, OrderCellProtocol {
    
    var orderTitleObject : OrderTitleObject!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    func updateCellWithObject(object: Any) {
        orderTitleObject = object as? OrderTitleObject
        titleLabel.text = orderTitleObject.title
    }
  
}

open class CatalogObject: NSObject , OrderCellObjectProtocol {
    
    var title :String = ""
    var items: Array<OrderCheckObject> = []
    var selected: OrderCheckObject?
    var id: Int!
    var type: String!
    
    public func getIdentifire() -> String {
        return "OrderTitleCell"
    }
    public func getType()-> String{
        return type
    }
    public func getValues()-> [String]{
        if selected != nil {
            return [String(selected!.getId())]
        } else {
            return []
        }
    }
    public func getId()-> Int{
        return id
    }
}

open class OrderTitleObject: NSObject , OrderCellObjectProtocol {
    
    var title :String = ""
    var items: Array<OrderCheckObject> = []
    var id: Int!
    var type: String!
    
    public func getIdentifire() -> String {
        return "OrderTitleCell"
    }
    public func getType()-> String{
        return type
    }
    public func getValues()-> [String]{
        return items.filter { (object) -> Bool in
            return object.isChecked
        }.map { (object) -> String in
            return String(object.id)
        }
    }
    public func getId()-> Int{
        return id
    }
}
