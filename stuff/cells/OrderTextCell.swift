//
//  OrderTextCell.swift
//  stuff
//
//  Created by Victor Roslyakov on 26.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import UIKit

class OrderTextCell: UITableViewCell, OrderCellProtocol,UIPickerViewDelegate, UIPickerViewDataSource {
 
    @IBOutlet weak var orderTextField: UITextField!
    
    var orderTextObject : OrderTextObject!
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    func updateCellWithObject(object: Any) {
        orderTextObject = object as? OrderTextObject
        orderTextField.placeholder = orderTextObject.title
        orderTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        switch orderTextObject.type {
        case "Catalog":
            createPickerView()
            dismissPickerView()
            break;
        case "Number":
            orderTextField.keyboardType = .numberPad
        case "Phone":
            orderTextField.keyboardType = .numberPad
        case "Email":
            orderTextField.keyboardType = .asciiCapable
        default:
            break;
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
     }
     
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return orderTextObject.refs.count
     }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedRef : refObject = orderTextObject.refs[row]
        orderTextField.text = selectedRef.value
        orderTextObject.inputText = selectedRef.value
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let selectedRef : refObject = orderTextObject.refs[row]
        return selectedRef.value
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        orderTextObject.inputText = textField.text ?? ""
    }
    func createPickerView() {
           let pickerView = UIPickerView()
           pickerView.delegate = self
           orderTextField.inputView = pickerView
       }
       
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: "dismissKeyboard")
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        orderTextField.inputAccessoryView = toolBar
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        orderTextField.endEditing(true)
    }
}

open class OrderTextObject: NSObject , OrderCellObjectProtocol {

    var inputText = ""
     var title :String = ""
     var isChecked = false
     var id = 0
     var type = ""
    var refs = Array<refObject>()
    
    public func getIdentifire() -> String {
        return "OrderTextCell"
    }
    
    public func getType() -> String {
        return type
    }
    public func getValues()-> [String]{
        if type == "Catalog"{
            var tempRefValue = ""
            for ref in refs {
                if ref.value == inputText{
                    tempRefValue = String(ref.id)
                }
            }
            return [tempRefValue]
        }else{
            return [inputText]
        }
    }
    public func getId()-> Int{
        return id
    }
}
