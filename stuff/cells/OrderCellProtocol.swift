//
//  OrderCellProtocol.swift
//  stuff
//
//  Created by Victor Roslyakov on 26.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation

public protocol OrderCellProtocol{
    func updateCellWithObject(object : Any)
}
public protocol OrderCellObjectProtocol{
    func getIdentifire()-> String
    func getType()-> String
    func getValues()-> [String]
    func getId()-> Int
}
