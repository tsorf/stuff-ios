//
//  ArDelo.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import ObjectMapper

class ArDelo : Mappable {
    
    var Filial: String = ""
    var Category: String = ""
    var Struct_Podr: String = ""
    var Department: String = ""
    var POS: String = ""
    var Date_Begin_work: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Filial    <- map["Filial"]
        Category         <- map["Category"]
        Struct_Podr      <- map["Struct_Podr"]
        Department       <- map["Department"]
        POS  <- map["POS"]
        Date_Begin_work  <- map["Date_Begin_work"]
    }
}
