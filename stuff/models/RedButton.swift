//
//  RedButton.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import ObjectMapper

class RedButton : Mappable {
    
    var GetForm : GetForm!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        GetForm <- map["GetForm"]
    }
}
class GetForm : Mappable {
    
    var FormId: Int = 0
    var FormName: String = ""
    var StepNames: [String : String] = [:]
    var Inputs : [Input]!
    var Refs: [String : [refObject]] = [:]
    var env: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        FormId <- map["FormId"]
        FormName <- map["FormName"]
        StepNames <- map["StepNames"]
        Inputs <- map["Inputs"]
        Refs <- map["Refs"]
        env <- map["env"]
    }
}

