//
//  FullProfile.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import ObjectMapper

public class FullProfile : Mappable {
    var profile : Profile?
    var links: [String : Link] = [:]
    var books: [String : Book] = [:]
    var spravki : Spravki?
    var redButton : RedButton?
    var aup: String?
    var trud: String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        profile <- map["profile"]
        links <- map["links"]
        books <- map["books"]
        spravki <- map["spravki"]
        redButton <- map["redButton"]
        aup <- map["aup"]
        trud <- map["trud"]
    }
    
}

