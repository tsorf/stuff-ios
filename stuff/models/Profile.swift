//
//  Profile.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import ObjectMapper
//import EasyMapping

class Profile : Mappable {
    
      var Ar_Delo : [ArDelo]?
      var DataRojdeniya: String = ""
      var Familiya: String!
      var Imya: String = ""
      var Otchestvo: String = ""
      var LoginAD: String = ""
      var Photo: String = ""
      var sex: Int = 0
      var Email: String = ""
      var GroupsAD : [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Ar_Delo <- map["Ar_Delo"]
        DataRojdeniya <- map["DataRojdeniya"]
        Familiya <- map["Familiya"]
        Imya <- map["Imya"]
        Otchestvo <- map["Otchestvo"]
        LoginAD <- map["LoginAD"]
        Photo <- map["Photo"]
        sex <- map["sex"]
        Email <- map["Email"]
        GroupsAD <- map["GroupsAD"]
    }
    
}

