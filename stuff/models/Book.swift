//
//  Books.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import ObjectMapper

class Book : Mappable{
    
    var title: String = ""
    var href: String = ""
    var img: String = ""
    
    required init?(map: Map) {
           
    }
       
    func mapping(map: Map) {
        title <- map["title"]
        href <- map["href"]
        img <- map["img"]
    }
}
