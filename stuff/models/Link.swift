//
//  Links.swift
//  stuff
//
//  Created by Victor Roslyakov on 23.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import ObjectMapper

class Link : Mappable{
    
    var title: String = ""
    var href: String = ""
    var icon: String = ""
    
    required init?(map: Map) {
           
    }
       
    func mapping(map: Map) {
        title <- map["title"]
        href <- map["href"]
        icon <- map["icon"]
    }
}
