//
//  spravki.swift
//  stuff
//
//  Created by Victor Roslyakov on 25.01.2020.
//  Copyright © 2020 Victor Roslyakov. All rights reserved.
//

import Foundation
import ObjectMapper

class Spravki : Mappable {
    
    var Content : Content!
    var Ordered : [Spravka]!
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Content <- map["Content"]
        Ordered <- map["Ordered"]
    }
}

class Form: Mappable {
    
    var Inputs: [Input]?
    var Refs: [String : [refObject]] = [:]
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        Inputs <- map["Inputs"]
        Refs <- map["Refs"]
    }
}

class Input: Mappable {

    var type: String = ""
    var name: String = ""
    var label: String = ""
    var fieldId: Int = 0
    var code: String = ""
    var ref: String = ""
    var value_embed: String = ""
    var readonly: Bool = false
    var hidden: String = ""
    
    required init?(map: Map) {
    }
       
    func mapping(map: Map) {
        type <- map["type"]
        name <- map["name"]
        label <- map["label"]
        fieldId <- map["fieldId"]
        code <- map["code"]
        ref <- map["ref"]
        value_embed <- map["value_embed"]
        readonly <- map["readonly"]
        hidden <- map["hidden"]
    }
}

class refObject: Mappable {
    
    var id: Int = 0
    var value: String = ""
    
    required init?(map: Map) {
    }
       
    func mapping(map: Map) {
        id <- map["id"]
        value <- map["value"]
    }
}

class Content : Mappable {
    
    var Forms: [String : Form] = [:]
    var List : [List]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Forms <- map["Forms"]
        List <- map["List"]
    }
}

class List: Mappable {
    
    var type: String = ""
    var name: String = ""
    var id: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        name <- map["name"]
        id <- map["id"]
    }
}
class Spravka: Mappable {
    
    var id: String = ""
    var type: String = ""
    var name: String = ""
    var date: String = ""
    var copies: Int = 0
    var result: String = ""
    var status: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        name <- map["name"]
        date <- map["date"]
        copies <- map["copies"]
        result <- map["result"]
        status <- map["status"]
    }
}
